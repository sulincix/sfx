#!/bin/bash
point=$(mktemp)
offset=186
rm -rf $point
mkdir $point
mount -t auto -o loop,offset=$offset $0 $point
[ -x "$point/AppRun" ] && exec $point/AppRun $*
umount -Rf $point
exit 0
